//#Manual Tasks
//##Define a function which runs the manual script
function run_content_script_v1() {
	//##Execute content script in active tab
	browser.tabs.executeScript({
		file: "/elements/manual.js"
	});
}
//##When extension button is clicked, run function which runs the manual script
browser.browserAction.onClicked.addListener(run_content_script_v1);